# Wine compiled for Richard Burns Rally 


Wine project compiled for Richard Burns Rally with FFB working.

Source code is taken from Kron4ek repo ---> https://github.com/Kron4ek/wine-tkg

To make Force Feedback work in RallySimFans Richard Burns Rally this patch is applied ---> https://bugs.winehq.org/show_bug.cgi?id=52714.

All credits go to the respective owners of the code.

How to:

1. Download the archive.
2. Unpack it to your Bottles data/runners directory.
3. Start Bottles and choose wine-9.7-staging-tkg as a runner in your RBR bottle.
4. Set DXVK to 2.3.1.
5. Set VKD3D to vkd3d-proton-2.12.
6. I had to also lower "Parallel controller input rate" and "Parallel FFB effects" rate. Both to 150Hz in RSF Launcher.
7. Start game and enjoy Force Feedback.

Tested on Fedora 40 beta and Bottles installed as Flatpak with Thrustmaster T150RS wheel.
I don't know whether this will work with other wheels but you can try.
Wheel sometimes does not work in game in controller settings (test force feedback) but it works in game when you go to the special stage.


## Running RBR on pure x86_64 Linux (64bit)

With new Wine experimental feature - WOW64 (Windows on Windows64) it is possible to run RBR on pure 64bit Linux installation. Without using 32bit libraries.
If you want to try it - use Wine compiled with WOW64 support (wine-#.#-staging-wow64-amd64.tar.xz). It includes Force Feedback patch.

For now Wine project states that it can be a bit slower. My experience is that the game loads faster, special stages load faster but the game itself indeed may be a bit slower.
